// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

#include <iostream> // cin, cout
#include <sstream>  // istringstream
#include <string>   // getline, string

using namespace std;

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return lhs._p == rhs._p;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------
        // pre increment
        iterator& operator ++ () {
            int blockSize = *_p;
            _p = reinterpret_cast<int*>(reinterpret_cast<char*>(_p) + abs(blockSize) + 8);
            return *this;
        }

        // -----------
        // operator ++
        // -----------
        // post increment
        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------
        // pre decrement
        iterator& operator -- () {
            int blockSize =  *(reinterpret_cast<int*>(reinterpret_cast<char*>(_p) - 4)); // get block size of the block to the left
            _p = reinterpret_cast<int*>(reinterpret_cast<char*>(_p) - abs(blockSize) - 8);
            return *this;
        }

        // -----------
        // operator --
        // -----------
        // post decrement
        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
        friend class my_allocator;
        friend void allocator_solve(istream& sin, ostream& sout);
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return lhs._p == rhs._p;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {
            int blockSize = *_p;
            _p = reinterpret_cast<const int*>(reinterpret_cast<const char*>(_p) + abs(blockSize) + 8); // 2 sentinels so 8
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {
            int blockSize =  *(reinterpret_cast<const int*>(reinterpret_cast<const char*>(_p) - 4));
            _p = reinterpret_cast<const int*>(reinterpret_cast<const char*>(_p) - abs(blockSize) - 8); // 2 sentinels so 8
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
        friend class my_allocator;
        friend void allocator_solve(istream& sin, ostream& sout);
    };

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * We test for valid by making sure that the headers and footers (sentinels) are correct, we can get to the
     * next block, we can get to the end, and that there is not two consequtive free blocks.
     */
    bool valid () const {
        my_allocator::const_iterator begin_ = begin();
        my_allocator::const_iterator end_ = end();

        while(begin_ != end_) { // making sure we get to the end properly
            my_allocator::const_iterator temp = begin_;
            ++temp;
            if (temp != end_) { // because if we're at the end, that's not actually a block - we should not do *
                if (*temp > 0 && *begin_ > 0) { // make sure there is not two free blocks in a row
                    return false;
                }
            }
            --temp;
            if(temp != begin_) { // checking that sentinels match - namely that the footer is same as header
                return false;
            }
            ++begin_;
        }
        return true;
    }

public:
    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        (*this)[0]   = N-8;
        (*this)[N-4] = N-8;
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type n) {
        if (n == 0) { // throw bad alloc when n is zero
            std::bad_alloc exception;
            throw exception;
        }
        int numBytes = n * sizeof(T);
        iterator begin_ = begin();
        iterator end_ = end();

        while (begin_ != end_) {
            int size = *begin_;

            if (size >= numBytes) { // first fit policy

                if (size - numBytes < (8 + sizeof(T))) { // 4 size 4 -> 4 >=numBytes 4
                    int* p = begin_._p;
                    *(reinterpret_cast<int*>(reinterpret_cast<char*>(p) + *p + 4)) *= -1; // footer sentinel
                    *p *= -1; // header sentinel
                    assert(valid());
                    return reinterpret_cast<pointer>(reinterpret_cast<char*>(p) + 4);
                } else { // 4 size 4 -> 4 numBytes 4 4 space 4, space = size - numBytes - 8
                    int* p = begin_._p;
                    int* q_footer = reinterpret_cast<int*>(reinterpret_cast<char*>(p) + *p + 4);
                    *p = -1 * numBytes;
                    int* p_footer = reinterpret_cast<int*>(reinterpret_cast<char*>(p) + abs(*p) + 4);
                    *(p_footer) = -1 * numBytes;
                    int* q = reinterpret_cast<int*>(reinterpret_cast<char*>(p_footer) + 4);
                    *q = size - numBytes - 8; // left-over bytes
                    *q_footer = size - numBytes - 8;
                    assert(valid());
                    return reinterpret_cast<pointer>(reinterpret_cast<char*>(p) + 4);
                }
            }
            ++begin_;
        }
        assert(valid());
        return nullptr;
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // merge_blocks
    // ----------
    /**
    * O(1) in space
    * O(1) in time
    * precondition: both header blocks passed in must be free (>0)
    * merges together the two header blocks and returns a pointer to the sentinel (header) of the new block
    */
    int* merge_blocks (int* header_one, int* header_two) {
        assert(*header_one > 0 && *header_two > 0); // assert that we have two free blocks
        int* left_header = min(header_one, header_two);
        int* right_header = max(header_one, header_two);
        int new_size = *left_header + *right_header + 8; // absorb one sentinel from each header so +8
        int* right_footer = reinterpret_cast<int*>(reinterpret_cast<char*>(right_header) + 4 + *right_header); // get to the footer
        *left_header = new_size;
        *right_footer = new_size;
        return left_header;
    }


    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * My documentation:
     * p points to a block of storage previously allocated with my_allocator::allocate()
     * n is the number of elements allocated on the call to my_allocator::allocate() for
     * this block of storage
     */
    void deallocate (pointer p, size_type n) {
        int* header_p = reinterpret_cast<int*>(reinterpret_cast<char*>(p) - 4); // points to the sentinel
        int num_elements = abs(*(header_p) / ((int) sizeof(value_type))); // T = value_type
        if (*header_p > 0 || header_p < begin()._p || header_p >= end()._p || num_elements != (int) n) { // if not busy or out of bounds or inconsistent n
            throw invalid_argument("");
        }
        int* l_header_p = nullptr;
        int* r_header_p = nullptr;

        iterator left_block(header_p);
        left_block--; // so it goes to the left block
        if (header_p != begin()._p) { // if the current block is not the header block of the entire array
            l_header_p = left_block._p;
            assert(left_block._p < end()._p);
        }
        iterator right_block(header_p);
        right_block++; // so it goes to the right block
        if (right_block._p < end()._p) { // if the block to the right is a real block
            r_header_p = right_block._p;
            assert(right_block._p >= begin()._p);
        }
        *(reinterpret_cast<int*>(reinterpret_cast<char*>(header_p) + 4 + abs(*header_p))) *= -1; // make footer say it's free
        *header_p *= -1;
        assert(*header_p > 0); // assert that header is free
        assert(*(reinterpret_cast<int*>(reinterpret_cast<char*>(header_p) + 4 + abs(*header_p))) > 0); // assert that footer says free too
        if (l_header_p != nullptr && *l_header_p > 0) { // if left block is not nullptr and it's free then can merge
            header_p = merge_blocks(l_header_p, header_p);
        }
        if (r_header_p != nullptr && *r_header_p > 0) {
            header_p = merge_blocks(header_p, r_header_p);
        }
        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(&(*this)[0]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return const_iterator(&(*this)[0]);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        return iterator(&(*this)[N]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return const_iterator(&(*this)[N]);
    }
};

#endif // Allocator_h
