// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Allocator.hpp"
#include <sstream>  // istringstream
#include <string>   // getline, string


using namespace std;
// ----
// main
// ----

int allocator_read_number (istream& sin) {
    string s;
    getline(sin, s);
    stringstream stream(s);
    int num;
    stream >> num; // num will have the number we're reading
    return num;
}


void allocator_solve (istream& sin, ostream& sout) {
    using allocator_type = my_allocator<double, 1000>; // because 8 Byte objects (so double should work)
    using value_type     = typename allocator_type::value_type;
    using pointer        = typename allocator_type::pointer;
    using iterator       = typename allocator_type::iterator;

    string s;
    int num_tests = allocator_read_number(sin);
    getline(sin, s); // skip the blank line after the number of tests

    for (int i = 0; i < num_tests; ++i) {
        allocator_type x;
        while(getline(sin, s) && s.length() != 0) {
            int data;
            istringstream sin(s); // name of the istringstream is sin
            sin >> data;
            assert(data != 0);
            if (data > 0) {
                const pointer begin = x.allocate(data); // because data = the number of objects
                pointer ptr = begin;

                for (int j = 0; j < data; ++j) {
                    x.construct(ptr, 0.0);
                    ++ptr;
                }
            }
            else { // data < 0 case
                iterator begin = x.begin();
                int count = 0;

                while (count != abs(data)) {
                    if (*begin < 0) { // if we found a busy block
                        ++count;
                    }
                    if (count != abs(data)) { // don't increment begin once we found target busy block
                        ++begin;
                    }
                }
                int num_elements = abs((*begin) / ((int)sizeof(value_type))); // need to abs since *begin is negative (it is not free)
                value_type* arr = reinterpret_cast<value_type*>(reinterpret_cast<char*>(begin._p) + 4); // cast start of block into a value_type*
                // note: value_type* = pointer

                for (int j = 0; j < num_elements; ++j) {
                    x.destroy(&(arr[j]));
                }
                x.deallocate((pointer)(reinterpret_cast<char*>(begin._p) + 4), num_elements); // + 4 to get to 'user' pointer from node pointer,
                // second param is the number of elements we allocated
            }

        }
        iterator begin_ = x.begin(); // print out the final result (block sentinels)
        iterator end_ = x.end();
        bool first_time_done = false;

        while (begin_ != end_) {
            if (first_time_done) {
                sout << " "; // spaces inbetween numbers
            }
            sout << *begin_;
            ++begin_;
            first_time_done = true;
        }
        sout << "\n";
    }


}

int main () {
    using namespace std;
    /*
    your code for the read eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    the acceptance tests are hardwired to use my_allocator<double, 1000>
    */
    allocator_solve(cin, cout);

    return 0;
}
