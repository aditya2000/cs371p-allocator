// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator
#include <vector>

#include "gtest/gtest.h"

#include "Allocator.hpp"

TEST(AllocatorFixture, test0) {
    bool successful = true;
    using allocator_type = std::allocator<int>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
    else {
        successful = false;
    }
    ASSERT_TRUE(successful);
}

TEST(AllocatorFixture, test1) {
    bool successful = true;
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
    else {
        successful = false;
    }
    ASSERT_TRUE(successful);
}


TEST(AllocatorFixture, test2) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;                                            // read/write
    ASSERT_EQ(x[0], 992);
}

TEST(AllocatorFixture, test3) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    const allocator_type x;                                      // read-only
    ASSERT_EQ(x[0], 992);
}

TEST(AllocatorFixture, test4) {
    using allocator_type = my_allocator<int, 1000>;
    using iterator       = typename allocator_type::iterator;
    int arr[9] = {8,0,0,8,12,0,0,0,12};
    vector<int> observed{};
    vector<int> expected{8,12};
    iterator iter(arr);
    iterator end(&(arr[9]));
    while(iter != end) {
        observed.push_back(*iter);
        ++iter;
    }
    ASSERT_EQ(expected, observed);
    iterator iter_array(arr);
    *iter_array = 10;
    ASSERT_EQ(*iter_array, 10);
}

TEST(AllocatorFixture, test5) {
    using allocator_type = my_allocator<int, 1000>;
    using iterator       = typename allocator_type::iterator;
    int arr[9] = {-8,0,0,-8,12,0,0,0,12};
    vector<int> observed{};
    vector<int> expected{12,-8};
    iterator iter(&(arr[4]));
    iterator start(arr);
    do {
        observed.push_back(*iter);
        --iter;
    } while (iter != start);
    observed.push_back(*iter);
    ASSERT_EQ(expected, observed);
    *iter = 20;
    ASSERT_EQ(20, *iter);
}

TEST(AllocatorFixture, test6) {
    using allocator_type = my_allocator<int, 1000>;
    using const_iterator = typename allocator_type::const_iterator;
    int arr[9] = {8,0,0,8,12,0,0,0,12};
    vector<int> observed{};
    vector<int> expected{8,12};
    const_iterator iter(arr);
    const_iterator end(&(arr[9]));
    while(iter != end) {
        observed.push_back(*iter);
        ++iter;
    }
    ASSERT_EQ(expected, observed);
}


TEST(AllocatorFixture, test7) {
    using allocator_type = my_allocator<int, 1000>;
    using const_iterator       = typename allocator_type::const_iterator;
    int arr[9] = {-8,0,0,-8,12,0,0,0,12};
    vector<int> observed{};
    vector<int> expected{12,-8};
    const_iterator iter(&(arr[4]));
    const_iterator start(arr);
    do {
        observed.push_back(*iter);
        --iter;
    } while (iter != start);
    observed.push_back(*iter);
    ASSERT_EQ(expected, observed);
}

TEST(AllocatorFixture, test8) {
    using allocator_type = my_allocator<int, 1000>;
    using iterator       = typename allocator_type::iterator;
    allocator_type x;
    ASSERT_EQ(reinterpret_cast<int*>(&(*(x.begin()))), &(x[0]));
    *x.begin() =  32;
    ASSERT_EQ(*x.begin(), 32);
}

TEST(AllocatorFixture, test9) {
    using allocator_type = my_allocator<int, 1000>;
    using iterator       = typename allocator_type::iterator;
    allocator_type x;
    ASSERT_EQ(reinterpret_cast<int*>(&(*(x.end()))), &(x[1000])); // because: [996][997][998][999] <-- last char in a[]
}

TEST(AllocatorFixture, test10) {
    using allocator_type = my_allocator<int, 1000>;
    using const_iterator       = typename allocator_type::const_iterator;
    const allocator_type x;
    ASSERT_EQ(reinterpret_cast<const int*>(&(*(x.begin()))), &(x[0]));
}

TEST(AllocatorFixture, test11) {
    using allocator_type = my_allocator<int, 1000>;
    using const_iterator       = typename allocator_type::const_iterator;
    const allocator_type x;
    ASSERT_EQ(reinterpret_cast<const int*>(&(*(x.end()))), &(x[1000])); // because: [996][997][998][999] <-- last char in a[]
}

TEST(AllocatorFixture, test12) {
    using allocator_type = my_allocator<int, 1000>;
    allocator_type x;
    int arr[6] = {4, 0, 4, 4, 0, 4}; // will become 16 0 0 0 0 16
    int* pointer_one = &(arr[0]);
    int* pointer_two = &(arr[3]);
    int* new_block = x.merge_blocks(pointer_one, pointer_two);
    ASSERT_EQ(pointer_one, new_block);
    ASSERT_EQ(*new_block, 16);
    ASSERT_EQ(arr[5], 16);
    ASSERT_EQ(arr[0], 16);
}

TEST(AllocatorFixture, test13) {
    using allocator_type = my_allocator<int, 1000>;
    allocator_type x;
    int arr[13] = {-4, 0, -4, 8, 0, 0, 8, 4, 0, 4, -4, 0, -4};
    int* pointer_one = &(arr[3]);
    int* pointer_two = &(arr[7]);
    int* new_block = x.merge_blocks(pointer_one, pointer_two);
    ASSERT_EQ(pointer_one, new_block);
    ASSERT_EQ(*new_block, 20);
    ASSERT_EQ(arr[3], 20);
    ASSERT_EQ(arr[9], 20);
    ASSERT_EQ(arr[0], -4); // make sure they stayed unchanged
    ASSERT_EQ(arr[2], -4);
    ASSERT_EQ(arr[10], -4);
    ASSERT_EQ(arr[12], -4);
}

TEST(AllocatorFixture, test14) {
    using allocator_type = my_allocator<int, 1000>;
    using iterator = typename allocator_type::iterator;
    int arr[9] = {-8,0,0,-8,12,0,0,0,12};
    iterator iter(&(arr[0]));
    iterator check = iter++;
    ASSERT_EQ(*check, -8);
    ASSERT_EQ(*iter, 12);
    *iter = 15;
    ASSERT_EQ(*iter, 15);
}

TEST(AllocatorFixture, test15) {
    using allocator_type = my_allocator<int, 1000>;
    using iterator = typename allocator_type::iterator;
    int arr[9] = {-8,0,0,-8,12,0,0,0,12};
    iterator iter(&(arr[4]));
    iterator check = iter--;
    ASSERT_EQ(*check, 12);
    ASSERT_EQ(*iter, -8);
    *iter = 15;
    ASSERT_EQ(*iter, 15);
}

TEST(AllocatorFixture, test16) {
    using allocator_type = my_allocator<int, 1000>;
    using const_iterator = typename allocator_type::const_iterator;
    int arr[9] = {-8,0,0,-8,12,0,0,0,12};
    const_iterator iter(&(arr[0]));
    const_iterator check = iter++;
    ASSERT_EQ(*check, -8);
    ASSERT_EQ(*iter, 12);
}

TEST(AllocatorFixture, test17) {
    using allocator_type = my_allocator<int, 1000>;
    using const_iterator = typename allocator_type::const_iterator;
    int arr[9] = {-8,0,0,-8,12,0,0,0,12};
    const_iterator iter(&(arr[4]));
    const_iterator check = iter--;
    ASSERT_EQ(*check, 12);
    ASSERT_EQ(*iter, -8);
}

TEST(AllocatorFixture, test18) {
    using allocator_type = my_allocator<int, 48>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    allocator_type x;
    const pointer a = x.allocate(5);
    pointer ptr = a;
    for (int j = 0; j < 5; ++j) {
        x.construct(ptr, j);
        ++ptr;
    }
    const pointer b = x.allocate(3);
    ptr = b;
    for (int j = 0; j < 3; ++j) {
        x.construct(ptr, j);
        ++ptr;
    }
    vector<int> observed{};
    vector<int> expected{-20,0,1,2,3,4,-20,-12,0,1,2,-12};
    int* traverse = &(*(x.begin()));
    for(int index = 0; index < 12; ++index) {
        observed.push_back(traverse[index]);
    }
    ASSERT_EQ(observed, expected);
}



TEST(AllocatorFixture, test19) {
    using allocator_type = my_allocator<int, 48>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;
    using iterator       = typename allocator_type::iterator;
    allocator_type x;
    const pointer a = x.allocate(5);
    pointer ptr = a;
    for (int j = 0; j < 5; ++j) {
        x.construct(ptr, j);
        ++ptr;
    }
    const pointer b = x.allocate(3);
    ptr = b;
    for (int j = 0; j < 3; ++j) {
        x.construct(ptr, j);
        ++ptr;
    }
    ptr = a;
    vector<int> observed{};
    vector<int> expected{40,40};
    int* arr = reinterpret_cast<int*>(ptr);
    for (int j = 0; j < 5; ++j) {
        x.destroy(&(arr[j]));
    }
    x.deallocate(ptr, 5);
    ptr = b;
    arr = reinterpret_cast<int*>(ptr);
    for (int j = 0; j < 3; ++j) {
        x.destroy(&(arr[j]));
    }
    x.deallocate(ptr, 3);
    iterator start = x.begin();
    int* end_ = reinterpret_cast<int*>(reinterpret_cast<char*>(&(*(start))) + 44);
    observed.push_back(*start);
    observed.push_back(*end_);
    ASSERT_EQ(observed, expected);

}


