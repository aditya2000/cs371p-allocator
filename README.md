# CS371p: Object-Oriented Programming Allocator Repo

* Name: Aditya Gupta, Varad Thorat

* EID: ag68834, vvt255

* GitLab ID: aditya2000, Varadthorat

* HackerRank ID: aditya20001, varadth

* Git SHA: d772e960b8306f4337524dc189378d80be1d339a

* GitLab Pipelines: https://gitlab.com/aditya2000/cs371p-allocator/-/pipelines

* Estimated completion time: 20 hours

* Actual completion time: 14 hours

* Comments: Aditya and Varad worked together for over 75% of the time, doing pair programming, so we qualify for the bonus 5 points.
